import { PrismaClient } from "@prisma/client";
// Data
import {
  cancellation_terms,
  events,
  payment_terms,
  ticket_purchase_terms,
  who_we_are_terms,
} from "./data";

const prisma = new PrismaClient();

async function main() {
  console.log(`Start seeding ...`);

  for (const event of events) {
    const result = await prisma.eventifyEvent.upsert({
      where: { id: event.id },
      update: {},
      create: event,
    });
    console.log(`Created event with id: ${result.id}`);
  }

  for (const item of who_we_are_terms) {
    const result = await prisma.termsWhoWeAre.upsert({
      where: { id: item.id },
      update: {},
      create: item,
    });
    console.log(`Created who we are with id: ${result.id}`);
  }

  for (const item of ticket_purchase_terms) {
    const result = await prisma.termsTicketPurchase.upsert({
      where: { id: item.id },
      update: {},
      create: item,
    });
    console.log(`Created ticket purchase term with id: ${result.id}`);
  }

  for (const item of payment_terms) {
    const result = await prisma.termsPayment.upsert({
      where: { id: item.id },
      update: {},
      create: item,
    });
    console.log(`Created payment term with id: ${result.id}`);
  }

  for (const item of cancellation_terms) {
    const result = await prisma.termsCancellation.upsert({
      where: { id: item.id },
      update: {},
      create: item,
    });
    console.log(`Created cancellation term with id: ${result.id}`);
  }

  console.log(`Seeding finished.`);
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });

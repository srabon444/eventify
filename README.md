# Eventify

Eventify is a web application that enables users to discover local events. Users can access event details, including location and other relevant information. Additionally, the option to purchase event tickets will be available soon (feature not yet implemented).


## [Live Application](https://eventify-pi.vercel.app/)

![App Screenshot 1](public/images/1.png)
![App Screenshot 2](public/images/2.png)
![App Screenshot 3](public/images/3.png)
![App Screenshot 4](public/images/4.png)

## Features

- View all events in ascending order by date.
- Search for events by location.
- Access event details.
- View event locations.

## Tech Stack
- Next.js
- React
- Typescript
- Javascript
- Tailwind
- Zod
- Framer Motion
- Prisma
- Postgres
- Vercel

**Hosting:**
- Vercel


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/srabon444/eventify.git
```

Install dependencies

```bash
  npm install
```

export const titles = {
  termsCondition: "Terms & Conditions",
  whoWeAre: "Who We Are",
  purchasingTickets: "Purchasing tickets or registrations",
  payment: "Payment",
  cancelledEvents: "Cancelled, varied or postponed Events",
};

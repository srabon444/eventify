import "server-only";
import { unstable_cache } from "next/cache";
import prisma from "@/lib/db";
import { notFound } from "next/navigation";
import { capitalize } from "@/lib/utils";

export const getEvents = unstable_cache(async (city: string, page = 1) => {
  const capitalizedCity = city === "all" ? undefined : capitalize(city);
  console.log(`Fetching events for city: ${capitalizedCity}, page: ${page}`);

  const events = await prisma.eventifyEvent.findMany({
    where: {
      city: capitalizedCity,
    },
    orderBy: {
      date: "asc",
    },
    take: 6,
    skip: (page - 1) * 6,
  });

  console.log(`Fetched events: ${JSON.stringify(events)}`);

  let totalCount;
  if (city === "all") {
    totalCount = await prisma.eventifyEvent.count();
  } else {
    totalCount = await prisma.eventifyEvent.count({
      where: {
        city: capitalizedCity,
      },
    });
  }

  return {
    events,
    totalCount,
  };
});

export const getEvent = unstable_cache(async (slug: string) => {
  const event = await prisma.eventifyEvent.findUnique({
    where: {
      slug: slug,
    },
  });

  if (!event) {
    return notFound();
  }
  return event;
});

export const getWhoWeAre = unstable_cache(async () => {
  return prisma.termsWhoWeAre.findMany();
});

export const getPurchasingTickets = unstable_cache(async () => {
  return prisma.termsTicketPurchase.findMany();
});

export const getPaymentTerms = unstable_cache(async () => {
  return prisma.termsPayment.findMany();
});

export const getCancellationTerms = unstable_cache(async () => {
  return prisma.termsCancellation.findMany();
});

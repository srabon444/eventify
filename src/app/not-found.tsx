import Image from "next/image";
import notFoundImage from "../../public/images/404.jpg";

export default function NotFound() {
  return (
    <main className="sm:w-[80%] w-[90%] mx-auto pt-20 pb-10">
      <section className="flex justify-center items-center w-full flex-col sm:gap-y-8 gap-y-2">
        <h1 className="sm:text-4xl text-2xl mb-8 text-center">
          The page you are looking for - Not Found!
        </h1>
        <Image
          className="shadow-lg shadow-amber-50 hover:scale-110 transition sm:max-w-[580px] max-w-[80%]"
          src={notFoundImage}
          alt="Not Found Image"
        />
      </section>
    </main>
  );
}

export default function PrivacyPolicy() {
  return (
    <main className="sm:w-[80%] w-[90%] mx-auto pt-20 pb-10">
      <section>
        <h1 className="text-2xl mb-8">Privacy Policy</h1>
        <h2 className=" mx-auto text-lg leading-8 text-white/75">
          When you buy something on this website, we collect personal
          information from you to fulfill the order. We may collect information
          like your:
        </h2>
      </section>

      <section className="w-full text-lg leading-8 mt-5">
        <ul className="list-disc list-inside text-white/75">
          <li>Billing address Details</li>
          <li>Shipping address Details</li>
          <li>Email address</li>
          <li>Name</li>
          <li>Phone number</li>
        </ul>
      </section>

      <p className="mt-5 text-md leading-8 text-white/75">
        We share this information with Squarespace, our online store hosting
        provider, so that they can provide website services to us. Our payment
        processor(s) [payment processor name(s)] will also collect payment
        information from you. You can read their privacy policy at [payment
        processor privacy policy URL]. As you go through checkout, this site may
        auto-complete your shipping and billing address by sharing what you type
        with the Google Places API and returning suggestions to you to improve
        your checkout experience.
      </p>
    </main>
  );
}

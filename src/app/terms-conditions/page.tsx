import {
  getWhoWeAre,
  getPurchasingTickets,
  getPaymentTerms,
  getCancellationTerms,
} from "@/lib/server-utils";
import { descriptions } from "@/resource/description";
import { titles } from "@/resource/title";
import TermsCondition from "@/components/terms-condition";

export default async function TermsConditionsPage() {
  const whoWeAre = await getWhoWeAre();
  const termsPurchasingTicket = await getPurchasingTickets();
  const termsPayment = await getPaymentTerms();
  const cancellationTerms = await getCancellationTerms();

  return (
    <main className="sm:w-[80%] w-[90%] mx-auto pt-20 pb-10">
      <section>
        <h1 className="text-2xl mb-8">{titles.termsCondition}</h1>
        <p className=" mx-auto text-lg leading-8 text-white/75">
          {descriptions.terms}
        </p>
      </section>

      <section className="w-full text-lg leading-8 mt-7">
        <ol className="list-decimal list-inside text-white/80">
          <TermsCondition title={titles.whoWeAre} conditionArray={whoWeAre} />
          <TermsCondition
            title={titles.purchasingTickets}
            conditionArray={termsPurchasingTicket}
          />
          <TermsCondition
            title={titles.payment}
            conditionArray={termsPayment}
          />
          <TermsCondition
            title={titles.cancelledEvents}
            conditionArray={cancellationTerms}
          />
        </ol>
      </section>
      <div></div>
    </main>
  );
}

import Image from "next/image";
import Link from "next/link";

export default function Logo() {
  return (
    <Link href="/">
      <Image
        src="https://i.ibb.co/3CJ62NN/eventify-high-resolution-logo-transparent-edit.png"
        alt="EVENTIFY logo"
        width={70}
        height={12}
      />
    </Link>
  );
}

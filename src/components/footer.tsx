import Link from "next/link";
import { gitlabUrl } from "@/lib/const";

export default function Footer() {
  let date = new Date().getFullYear();

  const routes = [
    {
      path: "/terms-conditions",
      name: "Terms & Conditions",
    },
    {
      path: "/privacy-policy",
      name: "Privacy Policy",
    },
  ];

  return (
    <footer className="mt-auto flex items-center justify-between h-16 border-t border-white/10 px-3 sm:px-9 text-xs text-white/25">
      <Link className="hover:text-white transition" href={gitlabUrl}>
        <small className="text-xs">
          &copy; {date} Ashraful. All Rights Reserved.
        </small>
      </Link>

      <ul className="flex gap-x-3 sm:gap-x-8">
        {routes.map((route) => (
          <li className="hover:text-white transition" key={route.path}>
            <Link href={route.path}>{route.name}</Link>
          </li>
        ))}
      </ul>
    </footer>
  );
}

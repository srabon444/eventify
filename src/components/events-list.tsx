import EventCard from "@/components/event-card";
import { getEvents } from "@/lib/server-utils";
import PaginationControls from "@/components/pagination-controls";

type EventsListProps = {
  city: string;
  page?: number;
};

export default async function EventsList({ city, page = 1 }: EventsListProps) {
  const { events, totalCount } = await getEvents(city, page);

  const previousPath = page > 1 ? `/events/${city}?page=${page - 1}` : "";
  const nextPath =
    totalCount > 6 * page ? `/events/${city}?page=${page + 1}` : "";

  return (
    <section className="max-w-[1100px] flex flex-col items-center px-[20px]">
      <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-10 justify-center w-full">
      {events.map((event) => (
        <EventCard key={event.id} event={event} />
      ))}
      </div>
      <PaginationControls previousPath={previousPath} nextPath={nextPath} />
    </section>
  );
}

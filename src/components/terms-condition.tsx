type TermsConditionProps = {
  title: string;
  conditionArray: Condition[];
};

type Condition = {
  id: number;
  content: string;
};

export default function TermsCondition({
  title,
  conditionArray,
}: TermsConditionProps) {
  return (
    <>
      <strong>
        <li>{title}</li>
      </strong>
      <ol className="list-inside text-white/75 pl-6 gap-y-2 mt-2 mb-6">
        {conditionArray.map((item: Condition) => (
          <li key={item.id} className="mb-2">
            {item.content}
          </li>
        ))}
      </ol>
    </>
  );
}
